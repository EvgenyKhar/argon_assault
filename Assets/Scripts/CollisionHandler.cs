﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [Tooltip("in s")][SerializeField] float levelLoadDelay = 1f;
    
   [Tooltip ("FX explosion of player")] [SerializeField] GameObject explosionFx;
    void OnTriggerEnter(Collider other)
    {

        StartDeathSequence();
    }

    private void StartDeathSequence()
    {
        //to PlayerController
        SendMessage("isDeathMessageRecived");
        explosionFx.SetActive(true);
        Invoke("LoadGameStart", levelLoadDelay);// <-string reference
    }

    private void LoadGameStart()
    {
        SceneManager.LoadScene(0);
    }
}
