﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void StartGame()
    {
       Invoke("LoadFirstLevel", 2f);
    }
    void LoadFirstLevel()
    {
        SceneManager.LoadScene(2);
    }

    public void OpenTitles()
    {
        Invoke("LoadTitles", 2f);
    }

    void LoadTitles()
    {
        SceneManager.LoadScene(1);
    }
    
}
