﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    enum GameState
    {
        playing,
        dying
    }
    private GameState gameState = GameState.playing;

    [Header("General")]
    [Tooltip("In ms^-1")] [SerializeField] float controllSpeed = 50;
    [Tooltip("In m")] [SerializeField] float xRangeFromCenter = 9.5f;
    [Tooltip("In m")] [SerializeField] float botMaxPos = -3.5f;
    [Tooltip("In m")] [SerializeField] float topMaxPos = 6.7f;
    [SerializeField] List<GameObject> guns;
    [SerializeField] List<GameObject> bigGuns;
    [SerializeField] VariableJoystick variableJoystick;

    private float xPos;
    private float xThrow;
    public float xOffset { get; private set; }

    private float yPos;
    private float yThrow;
    public float yOffset { get; private set; }
    bool isFireButton = false;

    private float pitch;
    private float yaw = 0f;
    private float roll = 0f;

    [Header("Position on screen Based")]
    [SerializeField] float positinonPitchFactor = -3.14f;
    [SerializeField] float positionYawFactor = 1.8f;
    [Header("Controll process based")]
    [SerializeField] float throwPitchFactor = -50f;
    [SerializeField] float throwRollFactor = -40f;


    void Start()
    {

    }

    void Update()
    {
        if(gameState==GameState.playing)
        {
            processTranslation();
            processRotation();
            processFiring();
        }
        

    }

    private void processFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            ActivateGuns(switchOn: true, guns);
        }
        else
        {
            ActivateGuns(switchOn: false, guns);
        }

        if (CrossPlatformInputManager.GetButton("Fire1"))
        {
            ActivateGuns(switchOn: true, bigGuns);
        }
        else
        {
            ActivateGuns(switchOn: false, bigGuns);
        }

    }

    private void ActivateGuns(bool switchOn, List<GameObject> guns)
    {
        foreach (var gun in guns)
        {
            ParticleSystem.EmissionModule em = gun.GetComponent<ParticleSystem>().emission;
            em.enabled = switchOn;
        }
    }

    //calling from CollisionHandler
    void isDeathMessageRecived() // <-string reference
    {
        gameState = GameState.dying;
    }
   

    private void processRotation()
    {
        //pitch зависит от текущей позиции - чуть поворачиваем, чтобы корабль смотрел также вперед, а не в центр кадра и добавляем
        //временное дополнительное смещение во время движения
        pitch = transform.localPosition.y * positinonPitchFactor + yThrow*throwPitchFactor;
        //yaw также зависит от текущей позиции, но движение вбок визуализируется за счет вращения roll, а не yaw.
        yaw =  transform.localPosition.x * positionYawFactor;
        roll = xThrow * throwRollFactor;

        transform.localRotation = Quaternion.Euler(pitch,yaw,roll);
    }

    private void processTranslation()
    {
        //framerate independent horizontal movement of ship
        
        if (Mathf.Abs(variableJoystick.Horizontal) < Mathf.Epsilon)
        {
            xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        }
        else
        {
            xThrow = variableJoystick.Horizontal;
        }
        xOffset = xThrow * Time.deltaTime * controllSpeed;
        xPos = transform.localPosition.x + xOffset;
        xPos = Mathf.Clamp(xPos, -xRangeFromCenter, xRangeFromCenter);

        //framerate independent vertical movement of ship
        if (Mathf.Abs(variableJoystick.Horizontal) < Mathf.Epsilon)
        {
            yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        }
        else
        {
            yThrow = variableJoystick.Vertical;
        }
        
        yOffset = yThrow * controllSpeed * Time.deltaTime;
        yPos = transform.localPosition.y + yOffset;
        yPos = Mathf.Clamp(yPos, botMaxPos, topMaxPos);

        transform.localPosition = new Vector3(xPos, yPos, transform.localPosition.z);
    }

    public void TouchFire()
    {
        isFireButton = true;
    }

    public void TouchFireOff()
    {
        isFireButton = false;
    }
}
