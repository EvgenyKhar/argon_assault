﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Tooltip("Enemy's cost in scores")][SerializeField] int scorePerHit = 12;
    [Tooltip("Enemy's explosion FX")] [SerializeField] GameObject deathFx;
    [Tooltip("in seconds")] [SerializeField] float deathTime = 1f;
    [Tooltip("_")] [SerializeField] Transform parent;
    [SerializeField] float healthPoint = 5;
    [SerializeField][Range (3, 10)] int armor = 3;
    BoxCollider boxCollider;


    ScoreBoard scoreBoard;
    SceneLoader sceneLoader;
    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>(); 
        scoreBoard = FindObjectOfType<ScoreBoard>();
        addNonTriggerBoxCollider();
    }

    private void addNonTriggerBoxCollider()
    {
        boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
        boxCollider.size =new Vector3(1.2f, 1.2f, 1.2f);
    }

    void OnParticleCollision(GameObject other)
    {
        DamageEnemy(other.name.ToString());
    }

    private void DamageEnemy(string bulletName)
    {
        int gunArmorReducer = 0;
        if(bulletName== "Gun Right" | bulletName== "Gun Left")
        {
            gunArmorReducer = 2;
        }
        else if(bulletName == "Power Gun Right" | bulletName == "Power Gun Left")
        {
            gunArmorReducer = 5;//todo получить это значение непосредственно от пушки, а не определять здесь.
        }

        healthPoint -= 1 / Mathf.Clamp(armor-gunArmorReducer, 1f, 10f);

        //для каждого попадания делаем свою анимацию
        //todo сделать свои FX и звук для отдельных попаданий
        GameObject fx = Instantiate(deathFx, transform.position, Quaternion.identity, parent);
        //если убили, снова запускаем анимацию и уничтожаем объект
        if (healthPoint <= 0)
        {
            scoreBoard.ScoreHit(scorePerHit);
            boxCollider.enabled = false;
            Invoke("deathSequence", deathTime);
        }
    }

    void deathSequence()
    {
        if(gameObject.name== "NewGameButton")
        {
            sceneLoader.OpenTitles();
        }
        GameObject fx =Instantiate(deathFx, transform.position, Quaternion.identity, parent);
        Destroy(gameObject);
    }

}
