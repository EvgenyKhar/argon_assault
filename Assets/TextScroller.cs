﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScroller : MonoBehaviour
{
    [SerializeField] int speed = 5;
    SceneLoader sceneLoader;
    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            sceneLoader.StartGame();
        }

        transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * speed, transform.position.z);
        if(Mathf.Abs( transform.position.y-1260f)<=Mathf.Epsilon)
        {
            sceneLoader.StartGame();
        }
    }
}
